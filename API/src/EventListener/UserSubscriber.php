<?php
namespace App\EventListener;

use App\Entity\User;
use Doctrine\ORM\Events;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mailer\MailerInterface;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs as EventLifecycleEventArgs;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserSubscriber implements \Doctrine\Common\EventSubscriber
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;
    /**
     * @var MailerInterface
     */
    private $mailer;

    public function __construct(UserPasswordEncoderInterface $encoder, MailerInterface $mailer)
    {
        $this->encoder= $encoder;
        $this->mailer=$mailer; 

    }
    
    /**
     * @inheritDoc
     */
    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
            Events::postPersist
        ];
    }

    /**
     * cette fonction se declenche juste après l'insertion d'un élement dans la BDD
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $this->sendWelcomeEmail($args);

    }

    /**
     * cette fonction se declenche juste avant l'insertion d'un élement dans la BDD
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $this->encodePassword($args);

    }
    /**
     * Perme d'encoder un motde passe dans la BSS juste avant l'insertion d'un User
     * @param LifecycleEventArgs $args
     */

    public function encodePassword (LifecycleEventArgs $args)
    {
        # 1. Recuperation de l'objet concerné
        $entity = $args->getObject();

        # 2. Si mon objet n'est pas une instance de "User" on quitte
        if (!$entity instanceof User){
            return;
        }

        #3. Sinon, on encode le mot de passe
        $entity->setPassword(
            $this->encoder->encodePassword(
                $entity,
                $entity->getPassword()
            )
            );
    }
    /**
     * Permet l'envoi 
     * TODO: Mettre en place un service dédié pour cela
     * @param LifecycleEeventArgs $args
     */
    private function sendWelcomeEmail (EventLifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof User){
            return;
        }
        $email = (new Email())
            ->from('noreply@patinette.fr')
            ->to($entity->getEmail())
            ->subject('Bienvenue sur notre application Patinette!')
            ->text('Bonjour, bienvenue chez Patinette!')
            ->html('<p>Bonjour, bienvenue chez Patinette!</p>');

        $this->mailer->send($email);

        // ...
    }
}