<?php

namespace App\Controller;

use Endroid\QrCode\QrCode;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TrottinetteController extends AbstractController
{
    public function LockTrottinette(int $id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $Trottinette = $em->find("App\Entity\Trottinette",$id);
        $Trottinette->setStatus(false);
        $em->flush();
        return new Response(sprintf("Trottinette with id = %s locked successefully",$Trottinette->getId()));
    }
    public function UnlockTrottinette(int $id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $Trottinette = $em->find("App\Entity\Trottinette",$id);
        $Trottinette->setStatus(true);
        $em->flush();
        return new Response(sprintf("Trottinette with id = %s unlocked successefully",$Trottinette->getId()));
    }
    public function QrCode(int $id)
    {
        $em = $this->getDoctrine()->getManager();
        $Trottinettes = $em->getRepository("App\Entity\Trottinette")->findAll();
        foreach($Trottinettes as $trottinette)
        {
            $path = "C:\wamp64\www\patinette_appli\API\src\Repository\QrCodes\QrCode";
            $url = sprintf("https://192.168.0.14:8000/api/Trottinettes/%s/Unlock",$trottinette->getId());
            $qrCode = new QrCode($url);
            $qrCode->writeFile($path.$trottinette->getId().".png");
        }
        return new Response("QrCode created for trottinettes");
    }
    public function GetLocation ():Response
    {
        $em = $this->getDoctrine()->getManager();
        $Trottinettes = $em->getRepository("App\Entity\Trottinette")->findAll();
        foreach($Trottinettes as $trottinette)
        {
            if ($trottinette->getStatus()==false){
                $lat=$trottinette->getLocation()->getLatitude();
                $long=$trottinette->getLocation()->getLongitude();
                //here we call the API 
                //AIzaSyBBxDREl2otGD5YHDTSXJ0ypt-TwhDX_Z8
            }
        }
        return new Response("Trottinettes' adresses are successfully displayed");
    }
}