<?php

namespace App\Controller;

use Endroid\QrCode\QrCode;
use App\Entity\Trottinette;
use App\Repository\TrottinetteRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LocationController extends AbstractController
{
    public function GetLocation ():Response
    {
        $em = $this->getDoctrine()->getManager();
        $Trottinettes = $em->getRepository("App\Entity\Trottinette")->findAll();
        foreach($Trottinettes as $trottinette)
        {
            if ($trottinette->getStatus()==false){
                $lat=$trottinette->getLocation()->getLatitude();
                $long=$trottinette->getLocation()->getLongitude();
                //here we call the API 
                //AIzaSyBBxDREl2otGD5YHDTSXJ0ypt-TwhDX_Z8
            }
        }
        return new Response("Trottinettes' adresses are successfully displayed");
    }
    static function getDistanceBetweenPoints($lat1, $lon1, $lat2, $lon2) : Float {
        $theta = $lon1 - $lon2;
        $miles = (sin(deg2rad($lat1)) * sin(deg2rad($lat2))) + (cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta)));
        $miles = acos($miles);
        $miles = rad2deg($miles);
        $miles = $miles * 60 * 1.1515;
        $kilometers = $miles * 1.609344;
        return $kilometers; 
    }

    function GetAvailableTrotinettes(Request $request) : Response
    {
        $em = $this->getDoctrine()->getManager();
        $Trottinettes = $em->getRepository("App\Entity\Trottinette")->findBy(['status'=>false]);
        $params = json_decode($request->getContent(), true);
        $tmp = array();
        foreach ($Trottinettes as $t) {
           if(self::getDistanceBetweenPoints($t->getLocation()->getLatitude(),$t->getLocation()->getLongitude(),$params['lat'],$params['long'])<$params['rayon']) array_push($tmp,$t->getId());
        };
        return new Response(json_encode($tmp));
    }
}