<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\LocationRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=LocationRepository::class)
 */
class Location
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     * 
     */
    private $latitude;

    /**
     * @ORM\Column(type="float")
     *
     */
    private $longitude;

    /**
     * @ORM\OneToMany(targetEntity=Trottinette::class, mappedBy="location")
     */
    private $trottinettes;

    public function __construct()
    {
        $this->trottinettes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * @return Collection|trottinette[]
     */
    public function getTrottinettes(): Collection
    {
        return $this->trottinettes;
    }

    public function addTrottinette(trottinette $trottinette): self
    {
        if (!$this->trottinettes->contains($trottinette)) {
            $this->trottinettes[] = $trottinette;
            $trottinette->setLocation($this);
        }

        return $this;
    }

    public function removeTrottinette(trottinette $trottinette): self
    {
        if ($this->trottinettes->removeElement($trottinette)) {
            // set the owning side to null (unless already changed)
            if ($trottinette->getLocation() === $this) {
                $trottinette->setLocation(null);
            }
        }

        return $this;
    }
}
