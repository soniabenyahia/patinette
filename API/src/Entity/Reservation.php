<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ReservationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ReservationRepository::class)
 */
class Reservation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $time_departure;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $time_arrival;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="reservations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Trottinette::class, inversedBy="reservations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $trottinette;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTimeDeparture(): ?\DateTimeInterface
    {
        return $this->time_departure;
    }

    public function setTimeDeparture(?\DateTimeInterface $time_departure): self
    {
        $this->time_departure = $time_departure;

        return $this;
    }

    public function getTimeArrival(): ?\DateTimeInterface
    {
        return $this->time_arrival;
    }

    public function setTimeArrival(?\DateTimeInterface $time_arrival): self
    {
        $this->time_arrival = $time_arrival;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUser(): ?user
    {
        return $this->user;
    }

    public function setUser(?user $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getTrottinette(): ?trottinette
    {
        return $this->trottinette;
    }

    public function setTrottinette(?trottinette $trottinette): self
    {
        $this->trottinette = $trottinette;

        return $this;
    }
}
