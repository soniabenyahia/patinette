<?php

namespace App\Repository;

use App\Entity\Trottinette;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Trottinette|null find($id, $lockMode = null, $lockVersion = null)
 * @method Trottinette|null findOneBy(array $criteria, array $orderBy = null)
 * @method Trottinette[]    findAll()
 * @method Trottinette[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TrottinetteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Trottinette::class);
    }
    
    public function GetAvailableTrotinettes()
    {
        $em= $this->getEntityManager();
        $query = $em->createQuery('SELECT * FROM App\Entity\Trottinette WHERE STATUS == FALSE ');
        $Trottinettes = $query->getResult();
        return $Trottinettes;
    }

    // /**
    //  * @return Trottinette[] Returns an array of Trottinette objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Trottinette
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
